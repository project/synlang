<?php

namespace Drupal\synlang\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Symfony\Component\Yaml\Exception\ParseException;
use Drupal\Component\Serialization\Yaml;

/**
 * Translation for this site.
 */
class TranslateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synlang_translate';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['source_container'] = [
      '#type' => 'container',
      'source' => [
        '#type' => 'textfield',
        '#title' => $this->t('Url with data for translations'),
        '#size' => 100,
        '#default_value' => 'https://git.synapse-studio.ru/d-org/synlang_translations/-/raw/master/main.yml',
      ],
      'aсtions' => [
        '#type' => 'actions',
        'ajax_check' => [
          '#type' => 'button',
          '#value' => $this->t('Check'),
          '#attributes' => ['class' => ['inline', 'btn-success']],
          '#ajax'   => [
            'callback' => '::ajaxCheck',
            'progress' => ['type' => 'throbber', 'message' => 'Проверяем.'],
          ],
        ],
        '#suffix' => "<pre id='check-result'></pre>",
      ],
    ];
    $header = [
      'code' => $this->t('Code'),
      'name' => $this->t('Name'),
    ];
    $langList = \Drupal::languageManager()->getLanguages();
    $options = [];
    if (!empty($langList)) {
      foreach ($langList as $key => $lang) {
        $options[$key] = [
          'code' => $key,
          'name' => $lang->getName(),
        ];
      }
    }
    $form['update_container'] = [
      '#type' => 'container',
      'content' => [
        '#type' => 'radios',
        '#required' => TRUE,
        '#title' => $this->t('Add new expressions'),
        '#default_value' => 1,
        '#options' => [
          1 => $this->t('Yes'),
          0 => $this->t('No'),
        ],
      ],
      'languages' => [
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $options,
        '#default_value' => $options,
      ],
      'aсtions' => [
        '#type' => 'actions',
        'ajax_update' => [
          '#type' => 'button',
          '#value' => $this->t('Update'),
          '#attributes' => ['class' => ['inline', 'btn-success']],
          '#ajax'   => [
            'callback' => '::ajaxUpdate',
            'progress' => ['type' => 'throbber', 'message' => 'Поехали.'],
          ],
        ],
        '#suffix' => "<pre id='update-result'></pre>",
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCheck(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValues()['source'];
    try {
      $data = Yaml::decode(file_get_contents($file));
      $answer = \Drupal::service('synlang.service')->countInfo($data);
    }
    catch (ParseException $exception) {
      $answer = $exception->getMessage();
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#check-result", $answer));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxUpdate(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValues()['source'];
    try {
      $data = Yaml::decode(file_get_contents($file));
      $answer = \Drupal::service('synlang.service')->updateTranslation($data, $form_state->getValues());
    }
    catch (ParseException $exception) {
      $answer = $exception->getMessage();
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#update-result", $answer));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
