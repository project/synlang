<?php

namespace Drupal\synlang\Drush\Commands;

use Drupal\Component\Serialization\Yaml;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class SynlangCommands extends DrushCommands {

  /**
   * Update translations from URL.
   *
   * @command synlang:slupdate
   * @aliases slupdate
   * @usage synlang:slupdate
   */
  #[CLI\Command(name: 'synlang:slupdate', aliases: ['slupdate'])]
  #[CLI\Argument(name: 'url', description: 'URL of transalion file.')]
  #[CLI\Usage(name: 'synlang:slupdate URL', description: 'Update translations from URL.')]
  public function slupdate($url) {
    $this->output()->writeln("Обновляем переводы ...");
    $langList = \Drupal::languageManager()->getLanguages();
    foreach ($langList as $key => $lang) {
      $langList[$key] = $key;
    }
    try {
      $data = Yaml::decode(file_get_contents($url));
      $settings = [
        'content' => 1,
        'languages' => $langList,
      ];
      $answer = \Drupal::service('synlang.service')->updateTranslation($data, $settings);
    }
    catch (ParseException $exception) {
      $answer = $exception->getMessage();
    }
    $this->output()->writeln($answer);
  }

  /**
   * Update config translations from URL.
   *
   * @command synlang:slupdate_config
   * @aliases slupdate_config
   * @usage synlang:slupdate_config
   */
  #[CLI\Command(name: 'synlang:slupdate_config', aliases: ['slupdate_config'])]
  #[CLI\Argument(name: 'url', description: 'URL of config transalion file.')]
  #[CLI\Usage(name: 'synlang:slupdate_config URL', description: 'Update config translations from URL.')]
  public function slupdateСonfig($url) {
    $this->output()->writeln("Обновляем переводы конфигов ...");
    $lang = \Drupal::languageManager()->getDefaultLanguage()->getId();
    try {
      $data = Yaml::decode(file_get_contents($url));
      $settings = [
        'content' => 1,
        'language' => $lang,
      ];
      $answer = \Drupal::service('synlang.service')->updateTranslationConfig($data, $settings);
    }
    catch (ParseException $exception) {
      $answer = $exception->getMessage();
    }
    $this->output()->writeln($answer);
  }

}
