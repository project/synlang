<?php

namespace Drupal\synlang\Service;

use Drupal\locale\SourceString;

/**
 * Service Update Translations.
 */
class UpdateTranslations {

  /**
   * {@inheritdoc}
   */
  public function countInfo($data) {
    $countWords = count($data);
    $langsData = [];
    $answer = "Всего переводов $countWords \n";
    foreach ($data as $values) {
      foreach ($values as $key => $value) {
        $langsData[$key] = isset($langsData[$key]) ? $langsData[$key] + 1 : 1;
      }
    }
    foreach ($langsData as $key => $value) {
      $answer .= "$key - $value \n";
    }
    return $answer;
  }

  /**
   * {@inheritdoc}
   */
  public function updateTranslation($data, $settings) {
    $langList = [];
    foreach ($settings['languages'] as $key => $value) {
      if ($value) {
        $langList[] = $key;
      }
    }
    $storage = \Drupal::service('locale.storage');
    $answer = "Вы добавили / обновили ";
    $count = 0;
    foreach ($data as $source => $values) {
      foreach ($values as $langcode => $translated_string) {
        $string = $storage->findString(['source' => $source]);
        if (is_null($string) && $settings['content']) {
          $string = new SourceString();
          $string->setString($source);
          $string->setStorage($storage);
          $string->save();
        }
        if (!is_null($string)) {
          if (in_array($langcode, $langList)) {
            $translation = $storage->createTranslation([
              'lid' => $string->lid,
              'language' => $langcode,
              'translation' => $translated_string,
            ])->save();
            $count++;
          }
        }
      }
    }
    return "$answer - $count";
  }

  /**
   * {@inheritdoc}
   */
  public function updateTranslationConfig($data, $settings) {
    $lang = $settings['language'];
    $answer = "Вы добавили / обновили ";
    $count = 0;
    foreach ($data as $cfg => $values) {
      $count++;
      foreach ($values as $config_key => $translations) {
        $config = \Drupal::service('config.factory')->getEditable($cfg);
        if ($config->get($config_key) !== NULL) {
          $config->set($config_key, $translations[$lang])->save();
        }
      }
    }
    return "$answer - $count";
  }

}
